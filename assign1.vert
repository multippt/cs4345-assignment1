//============================================================
// STUDENT NAME: Kwan Yong Kang Nicholas
// MATRIC NO.  : A0080933E
// NUS EMAIL   : a0080933@nus.edu.sg
// COMMENTS TO GRADER:
// 
//
//============================================================
//
// FILE: assign1.vert


varying vec3 ecPosition; // Vertex's position in eye space.
varying vec3 ecNormal;   // Vertex's normal vector in eye space.
varying vec3 ecTangent;  // Vertex's tangent vector in eye space.

attribute vec3 Tangent;  // Input vertex's tangent vector in model space.


void main( void )
{
	ecNormal = normalize(gl_NormalMatrix * gl_Normal);
	ecTangent = normalize(gl_NormalMatrix * Tangent);
	vec4 ecPosition4 = gl_ModelViewMatrix * gl_Vertex;
	ecPosition = vec3(ecPosition4) / ecPosition4.w;
	
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = ftransform();
}
