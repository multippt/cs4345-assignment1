//============================================================
// STUDENT NAME: Kwan Yong Kang Nicholas
// MATRIC NO.  : A0080933E
// NUS EMAIL   : a0080933@nus.edu.sg
// COMMENTS TO GRADER:
// 
//
//============================================================
//
// FILE: assign1.frag

//============================================================================
// Eye-space position and vectors for setting up a tangent space at the fragment.
//============================================================================

varying vec3 ecPosition;    // Fragment's 3D position in eye space.
varying vec3 ecNormal;      // Fragment's normal vector in eye space.
varying vec3 ecTangent;     // Fragment's tangent vector in eye space.


//============================================================================
// TileDensity specifies the number of tiles to span across each dimension when the
// texture coordinates gl_TexCoord[0].s and gl_TexCoord[0].t range from 0.0 to 1.0.
//============================================================================

uniform float TileDensity;  // (0.0, inf)


//============================================================================
// TubeRadius is the radius of the semi-circular mirror tubes that run along 
// the boundary of each tile. The radius is relative to the tile size, which 
// is considered to be 1.0 x 1.0.
//============================================================================

uniform float TubeRadius;  // (0.0, 0.5]


//============================================================================
// StickerWidth is the width of the square sticker. The entire square sticker 
// must appear at the center of each tile. The width is relative to the 
// tile size, which is considered to be 1.0 x 1.0.
//============================================================================

uniform float StickerWidth;  // (0.0, 1.0]


//============================================================================
// EnvMap references the environment cubemap for reflection mapping.
//============================================================================

uniform samplerCube EnvMap;


//============================================================================
// DiffuseTex1 references the wood texture map whose color is used to 
// modulate the ambient and diffuse lighting components on the non-mirror and
// non-sticker regions.
//============================================================================

uniform sampler2D DiffuseTex1;


//============================================================================
// DiffuseTex2 references the sticker texture map whose color is used to 
// modulate the ambient and diffuse lighting components on the sticker regions.
//============================================================================

uniform sampler2D DiffuseTex2;

void main()
{
    vec2 c = TileDensity * gl_TexCoord[0].st;
    vec2 p = fract( c ) - vec2( 0.5 );

    // Some useful eye-space vectors.
    vec3 ecNNormal = normalize( ecNormal );
    vec3 ecViewVec = -normalize( ecPosition );

	// Back view dot product between normal and view vector would be negative
	if (dot(ecNNormal, ecViewVec) < 0.0) 
	{
        //======================================================================
        // In here, fragment is backfacing or in the non-bump region.
        //======================================================================
		
		// Draw the background wood with phong shading (using half-vector)
		vec3 ecNormalizedNormal = -ecNNormal; // Negative, as back-facing
		vec3 lightPosition = vec3(gl_LightSource[0].position) / gl_LightSource[0].position.w;
		vec3 lightVector = normalize(lightPosition - ecPosition);
		vec3 halfVector = normalize(lightVector + ecViewVec);
		
		float NDotL = max(0.0, dot(ecNormalizedNormal, lightVector));
		float NDotH = max(0.0, dot(ecNormalizedNormal, halfVector));
		
		float shininessFactor = 0.0;
		if (NDotH > 0.0) {
			shininessFactor = pow(NDotH, gl_FrontMaterial.shininess);
		}
		
		vec3 texColor = texture2D(DiffuseTex1, gl_TexCoord[0].st).rgb;
		gl_FragColor = gl_FrontLightModelProduct.sceneColor
			+ vec4(texColor,1.0) * gl_LightSource[0].ambient * gl_FrontMaterial.ambient
			+ vec4(texColor,1.0) * gl_LightSource[0].diffuse * gl_FrontMaterial.diffuse * NDotL
			+ gl_LightSource[0].specular * gl_FrontMaterial.specular * shininessFactor;
    }
    else
    {
        //======================================================================
        // In here, fragment is front-facing and in the mirror-like bump region.
        //======================================================================

        vec3 N = ecNNormal;
        vec3 B = normalize( cross( N, ecTangent ) );
        vec3 T = cross( B, N );

        vec3 tanPerturbedNormal;  // The perturbed normal vector in tangent space of fragment.
        vec3 ecPerturbedNormal;   // The perturbed normal vector in eye space.
        vec3 ecReflectVec;        // The mirror reflection vector in eye space.

		vec2 tileCoordinate = fract(c); // Find coordinates relative to a tile
		int drawWood = 1; // Enable/disable background texture

		vec4 netColor;
		
		// Check if region is inside the tube
		float tubeBoundLeft = TubeRadius;
		float tubeBoundRight = 1.0 - TubeRadius;
		if (tileCoordinate.s < tubeBoundLeft || tileCoordinate.s > tubeBoundRight || tileCoordinate.t < tubeBoundLeft || tileCoordinate.t > tubeBoundRight) {
			// Decide which direction the tube is at
			int useVertical = 0;
			if (tileCoordinate.s < 0.5 && tileCoordinate.t < 0.5) { // Bottom-left				
				useVertical = (tileCoordinate.s > tileCoordinate.t)? 0 : 1;
			} else if (tileCoordinate.s >= 0.5 && tileCoordinate.t < 0.5) { // Bottom-right
				useVertical = ((1.0 - tileCoordinate.s) > (tileCoordinate.t))? 0 : 1;
			} else if (tileCoordinate.s < 0.5 && tileCoordinate.t >= 0.5) { // Top-left
				useVertical = ((1.0 - tileCoordinate.t) < (tileCoordinate.s))? 0 : 1;
			} else if (tileCoordinate.s >= 0.5 && tileCoordinate.t >= 0.5) { // Top-left
				useVertical = ((1.0 - tileCoordinate.t) < (1.0 - tileCoordinate.s))? 0 : 1;
			}
			
			float rangeValue = 0; // value 0~1, interpolation between 0 degree and 90 degree
			// Find the interpolation degree
			if (useVertical == 1) { // Tube is vertical
				rangeValue = tileCoordinate.s;
			} else {
				rangeValue = tileCoordinate.t;
			}
			if (rangeValue > 0.5) {
				rangeValue = rangeValue - 1.0;
			}
			rangeValue = rangeValue / TubeRadius; // rangeValue will always be <= TubeRadius
			
			tanPerturbedNormal = vec3(0.0, 0.0, 1.0);
			if (useVertical == 1) {
				tanPerturbedNormal.x = rangeValue;
			} else {
				tanPerturbedNormal.y = rangeValue;
			}
			
			mat3 tanECMatrix = mat3(T,B,N);
			ecPerturbedNormal = normalize(tanECMatrix * tanPerturbedNormal);
			ecNNormal = ecPerturbedNormal;
			
			// Reflection mapping
			drawWood = 0; // Set to 1 to see wood texture (to check normals)
			ecReflectVec = reflect(ecPosition, ecNNormal);
			netColor = textureCube(EnvMap, ecReflectVec);
		}
		
		// Phong lighting preparations
		vec3 lightPosition = vec3(gl_LightSource[0].position) / gl_LightSource[0].position.w;
		vec3 lightVector = normalize(lightPosition - ecPosition);
		vec3 halfVector = normalize(lightVector + ecViewVec);
		float NDotL = max(0.0, dot(ecNNormal, lightVector));
		float NDotH = max(0.0, dot(ecNNormal, halfVector));
		float shininessFactor = 0.0;
		if (NDotH > 0.0) {
			shininessFactor = pow(NDotH, gl_FrontMaterial.shininess);
		}

		// Check if fragment is inside sticker area
		float stickerBoundLeft = (1.0 - StickerWidth)/2.0;
		float stickerBoundRight = stickerBoundLeft + StickerWidth;
		if (stickerBoundLeft < tileCoordinate.s && tileCoordinate.s < stickerBoundRight && stickerBoundLeft < tileCoordinate.t && tileCoordinate.t < stickerBoundRight) {
			// Draw the sticker
		
			// Find sticker texture coordinates
			vec2 stickerTextureCoordinate = vec2((tileCoordinate.s - stickerBoundLeft), (tileCoordinate.t - stickerBoundLeft)) / StickerWidth;
			vec3 texColor = texture2D(DiffuseTex2, stickerTextureCoordinate.st).rgb;
			
			// Replace color with sticker texture (plus phong shading)
			netColor = gl_FrontLightModelProduct.sceneColor
				+ vec4(texColor,1.0) * gl_LightSource[0].ambient * gl_FrontMaterial.ambient
				+ vec4(texColor,1.0) * gl_LightSource[0].diffuse * gl_FrontMaterial.diffuse * NDotL
				+ gl_LightSource[0].specular * gl_FrontMaterial.specular * shininessFactor;
		} else if (drawWood == 1) {
			// Draw background wood otherwise
			
			vec3 texColor = texture2D(DiffuseTex1, gl_TexCoord[0].st).rgb;
			netColor = gl_FrontLightModelProduct.sceneColor
				+ vec4(texColor,1.0) * gl_LightSource[0].ambient * gl_FrontMaterial.ambient
				+ vec4(texColor,1.0) * gl_LightSource[0].diffuse * gl_FrontMaterial.diffuse * NDotL
				+ gl_LightSource[0].specular * gl_FrontMaterial.specular * shininessFactor;
		}
		
		gl_FragColor = netColor;
    }

}
